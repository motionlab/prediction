FROM python

ENV DEBIAN_FRONTEND="noninteractive" 

RUN  useradd -r -u 1001 appuser

# Basic stuff
RUN apt-get update                                               && \
    apt-get upgrade -y                                           && \
    apt-get install -y curl mc vim python imagemagick xmlstarlet && \
    rm -rf /var/lib/apt/lists/*

# Quickfix for imagemagig 6 config error
RUN sed -i '/<!-- <policy/d' /etc/ImageMagick-6/policy.xml 

RUN xmlstarlet ed --inplace -d '//comment()'                                                     /etc/ImageMagick-6/policy.xml && \ 
    xmlstarlet ed --inplace -u "/policymap/policy[@name='memory']/@value" -x 'string("4096MiB")' /etc/ImageMagick-6/policy.xml && \
    xmlstarlet ed --inplace -u "/policymap/policy[@name='area']/@value"   -x 'string("4096MiB")' /etc/ImageMagick-6/policy.xml && \
    xmlstarlet ed --inplace -u "/policymap/policy[@name='disk']/@value"   -x 'string("4GiB")'    /etc/ImageMagick-6/policy.xml 

COPY requirements.txt requirements.txt
RUN  pip  install -r  requirements.txt

RUN mkdir /app                  && \
    mkdir /home/appuser         && \
    chown appuser /app          && \ 
    chown appuser /home/appuser

WORKDIR /app
USER    appuser

ADD process.sh              /app/process.sh
ADD generate.py             /app/generate.py
ADD sendemail.py            /app/sendemail.py
ADD templates               /app/templates
ADD not_interested-24px.svg /app/not_interested-24px.svg
