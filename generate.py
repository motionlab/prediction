import sys
import jinja2
import datetime
from   os                     import walk
from   os                     import path
from   calendar               import monthrange
from   dateutil.relativedelta import relativedelta

iconpath = "https://icons.23-5.eu"

year  = datetime.date.today().year
month = datetime.date.today().month

month_name = {}
month_name[ 1] = "Jan"
month_name[ 2] = "Feb"
month_name[ 3] = "Mar"
month_name[ 4] = "Apr"
month_name[ 5] = "May"
month_name[ 6] = "Jun"
month_name[ 7] = "Jul"
month_name[ 8] = "Aug"
month_name[ 9] = "Sep"
month_name[10] = "Oct"
month_name[11] = "Nov"
month_name[12] = "Dec"

vars = {}
vars['title_date'] = "%s - %s" % (year, month)
vars['year']       = year
vars['month']      = month
vars['month_name'] = month_name[month]
vars['iconpath']   = iconpath

today_dt = datetime.datetime(year, month, 1, 12, 12, 12)

links = {}


for (dirpath, dirnames, filenames) in walk("/host/"):
    break

dirnames.sort(reverse=True)
pred=[]

for dir in dirnames[:5]:
    data = {}
    data['year']  = year
    data['month'] = dir[0:2]
    data['day']   = dir[3:5]
    data['hour']  = dir[6:8]

    for (_,_,c) in walk("/host/{}_{}_{}_test/comp/".format(data['month'] , data['day'], data['hour'])):
        break

    if c is not None:
        c.sort(reverse=False)
        data['comp'] = c
        name = []
        for i in c:
            name.append(i[10:18])
        data['comp_name'] = name

    for (_,_,c) in walk("/host/{}_{}_{}_test/rain_3h/".format(data['month'] , data['day'], data['hour'])):
        break

    if c is not None:
        c.sort(reverse=False)
        data['rain3h'] = c
        name = []
        for i in c:
            name.append(i[13:21])
        data['rain3h_name'] = name

    for (_,_,c) in walk("/host/{}_{}_{}_test/rain_tot/".format(data['month'] , data['day'], data['hour'])):
        break

    if c is not None:
        c.sort(reverse=False)
        data['raintot'] = c
        name = []
        for i in c:
            name.append(i[14:22])
        data['raintot_name'] = name

    for (_,_,c) in walk("/host/{}_{}_{}_test/thunderstorm_index/".format(data['month'] , data['day'], data['hour'])):
        break

    if c is not None:
        c.sort(reverse=False)
        data['thunder'] = c
        name = []
        for i in c:
            name.append(i[18:26])
        data['thunder_name'] = name

    pred.append(data)

templateLoader = jinja2.FileSystemLoader(searchpath="templates")
templateEnv    = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE  = "month.html"
template       = templateEnv.get_template(TEMPLATE_FILE)
outputText     = template.render(vars=vars, links=links, pred=pred)

with open(today_dt.strftime("/host/index.html"), 'w') as the_file:
    the_file.write(outputText)

