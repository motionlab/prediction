import os
import sys
import time
import requests

PREDICTION_PATH = "/host"
FROM            = "Ansis Weather Service <postmaster@23-5.eu>"
TO              = sys.argv[2]
KEY             = sys.argv[3]
dirlist         = next(os.walk(PREDICTION_PATH))[1]
dirlist.sort(reverse=True)
folder          = dirlist[0]
prename         = '_'.join(folder.split('_')[0:3])

HTML = """
<html>
<h1>Ansis Weather Service</h1>
<p>
Please find attached the weather prediction. They are calculated 4 times a day (0,6,12,18) o'clock and take apploximately 6 hours to be calculated.
</p>
<p>
Current predictions can be found at <a href='https://prediction.23-5.eu'>prediction.23-5.eu</a>. If you want to get current weather infromation have
a look at the MotionLab Weather Station at <a href='https://weather.23-5.eu'>weather.23-5.eu</a>. The DWD has also an very usefull radar map at
<a href='https://www.dwd.de/DWD/wetter/radar/radfilm_bbb_akt.gif'>www.dwd.de</a>.
</p>
<p>
Greetings
Ansi
<a href='https://ansi.23-5.eu'>ansi.23-5.eu</a>
</p>
</html>
"""

print(f"Folder to work with: {folder}")

if sys.argv[1] == "BER":
    print ("Berlin")
    add = "Ber"

if sys.argv[1] == "UED":
    print ("Uedersee")
    add = "Ude"

meteogram = open(f"{PREDICTION_PATH}/{folder}/{prename}_meteogram_{add}.png", 'rb')
temp      = open(f"{PREDICTION_PATH}/{folder}/comp.gif",                      'rb')
rain3h    = open(f"{PREDICTION_PATH}/{folder}/rain_3h.gif",                   'rb')
raintot   = open(f"{PREDICTION_PATH}/{folder}/rain_tot.gif",                  'rb')
thunder   = open(f"{PREDICTION_PATH}/{folder}/thunderstorm_index.gif",        'rb')

r = requests.post("https://api.mailgun.net/v3/23-5.eu/messages",
        auth=("api", KEY),
        files=[("attachment", meteogram),
               ("attachment", temp)],
        data={"from": "Ansis Weather Service<weatherprediction@23-5.eu>",
              "to": TO,
              "subject": "Weather Prediction with Meteogram and temp",
              "text": "Please find attached the weather prediction.",
              "html": HTML})

print(r)
time.sleep(60)

r = requests.post("https://api.mailgun.net/v3/23-5.eu/messages",
        auth=("api", KEY),
        files=[("attachment", rain3h),
               ("attachment", raintot)],
        data={"from": "Ansis Weather Service<weatherprediction@23-5.eu>",
              "to": TO,
              "subject": "Weather Prediction with rain",
              "text": "Please find attached the weather prediction.",
              "html": HTML})

print(r)
time.sleep(60)

r = requests.post("https://api.mailgun.net/v3/23-5.eu/messages",
        auth=("api", KEY),
        files=[("attachment", thunder)],
        data={"from": "Ansis Weather Service<weatherprediction@23-5.eu>",
              "to": TO,
              "subject": "Weather Prediction with Thunderstorm",
              "text": "Please find attached the weather prediction.",
              "html": HTML})

print(r)
