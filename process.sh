#!/bin/bash

for i in `find /host/ -maxdepth 1 -type d `
do
 echo ${i}
 cd ${i}

 if [ ! -f comp.gif ]
 then
    if [ -d comp ] 
    then
        if [ ! -z "$(ls -A comp)" ]
        then
         convert -delay 23 -loop 0 comp/*.png comp.gif
        else
         convert /app/not_interested-24px.svg comp.gif
        fi
    else
        convert /app/not_interested-24px.svg comp.gif
    fi
 fi

 if [ ! -f rain_3h.gif ]
 then
    if [ -d rain_3h ] 
    then
        if [ ! -z "$(ls -A rain_3h)" ]
        then
         convert -delay 23 -loop 0 rain_3h/*.png rain_3h.gif
        else
         convert /app/not_interested-24px.svg rain_3h.gif
        fi
    else
        convert /app/not_interested-24px.svg rain_3h.gif
    fi
 fi

 if [ ! -f rain_tot.gif ]
 then
    if [ -d rain_tot ] 
    then
        if [ ! -z "$(ls -A rain_tot)" ]
        then
         convert -delay 23 -loop 0 rain_tot/*.png rain_tot.gif
        else
         convert /app/not_interested-24px.svg rain_tot.gif
        fi
    else
        convert /app/not_interested-24px.svg rain_tot.gif
    fi
 fi

 if [ ! -f thunderstorm_index.gif ]
 then
    if [ -d thunderstorm_index ] 
    then
        if [ ! -z "$(ls -A thunderstorm_index)" ]
        then
         convert -delay 23 -loop 0 thunderstorm_index/*.png thunderstorm_index.gif
        else
         convert /app/not_interested-24px.svg thunderstorm_index.gif
        fi
    else
        convert /app/not_interested-24px.svg thunderstorm_index.gif
    fi
 fi

done

